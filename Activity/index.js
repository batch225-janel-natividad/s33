/*
Activity:
1. In the S28 folder, create an activity folder and an index.html and a script.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
11. Update a to do list item by changing the status to complete and add a date when the status was changed.
12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items
14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item
15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item
16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request
17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item
19. Export the Postman collection and save it in the activity folder.
20. Create a git repository named S28.
21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
22. Add the link in Boodle.
*/

//3.GET ALL 
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

//4.
titles = [];
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    data.map((todo) => titles.push(todo.title));
  });
console.log(titles);

//5
fetch('https://jsonplaceholder.typicode.com/todos/5')
.then((response) => response.json())
.then((json) => console.log(json));

//6
fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  body: JSON.stringify({
    title: 'Add an Item',
    completed: false
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
}).then((response) => response.json())
.then((json) => console.log(json));


//7
fetch('https://jsonplaceholder.typicode.com/todos',{
  method: 'POST',
  headers: {
		'Content-type': 'application/json'
	},
  body: JSON.stringify({
  	userId: 105,
  	id: 201, 
    title: 'New to do Item',
    completed: false
  })
}).then((response) => response.json())
.then((json) => console.log(json));

//8
fetch('https://jsonplaceholder.typicode.com/todos/1',{
  method: 'PUT',
  headers: {
		'Content-type': 'application/json'
	},
  body: JSON.stringify({
  	userId: 1,
  	id: 1, 
    title: 'Update an Item',
    completed: false
  })
}).then((response) => response.json())
.then((json) => console.log(json));

//9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  body: JSON.stringify({
    title: 'Update an Item using PUT',
    description: 'Learn how to use the Fetch API to make requests to a server',
    status: 'completed',
    dateCompleted: '2022-12-31',
    userId: 1
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
.then((response) => response.json())
.then((json) => console.log(json));

//10
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  body: JSON.stringify({
    title: 'Update using Patch'
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
.then((response) => response.json())
.then((json) => console.log(json));

//11
const dateCompleted = new Date().toISOString()
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  body: JSON.stringify({
    status: 'completed',
    dateCompleted
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
}).then((response) => response.json())
.then((json) => console.log(json));

//12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});

